\documentclass{article}

\usepackage{enumitem}
\usepackage{booktabs}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{lmodern}  % change font to Latin Modern
\let\counterwithin\relax
\let\counterwithout\relax
\usepackage{chngcntr} % counterwithin
\usepackage[compact]{titlesec} % titleformat
\usepackage{qrcode}
\usepackage{tabularx}
\usepackage{pdfpages}
\usepackage{hyperref}

\newtheorem{thm}{Theorem}
\newtheorem{lem}{Lemma}

\renewcommand\thesubsubsection{\alph{subsubsection}}
\titleformat{\section}{\normalfont\Large\bfseries}{Problem Set \thesection:}{1ex}{}
\titleformat{\subsection}{\normalfont\large\bfseries}{Exercise }{0ex}{}
\titleformat{\subsubsection}{\normalfont\bfseries}{-- (\thesubsubsection)}{1ex}{}
\counterwithin*{equation}{subsection}

\title{ MATH 813 -- Algebra I }
\author{ Paul Modlin }
\date{\today}

\begin{document}
\maketitle
\section{Chapter 7.1}
For all problems, let $R$ be a ring, unless otherwise stated.

\subsection{1}
By the definition of additive inverse, $(1 + -1)(1 + -1) = (0)(0) = 0$.
Now by the distributive property:
\begin{align*}
	(1 + -1)(1 + -1)
	&= 1(1 + -1) + -1(1 + -1) \\
	&= 1(1) + 1(-1) + -1(1) + (-1)(-1) \\
	&= 1 + -1 + -1 + {(-1)}^2 \\
	&= -1 + {(-1)}^2 \\
\end{align*}
So, ${(-1)}^2 + (-1) = 0$.
Therefore ${(-1)}^2$ is the additive inverse of $-1$.
So ${(-1)}^2 = 1$.
\subsection{2}
\begin{proof}
	Let $u$ be a unit of $R$.
	Then $\exists v$ such that $uv = 1$.
	By Problem 1,
	\begin{align*}
		1 = uv = [(u)(1)](v)
		&= \big[(u)[(-1)(-1)]\big]v \\
		&= \big[(u)(-1)\big]\big[(-1)(v)\big] \\
		&= (-u)(-v)
	\end{align*}
	So $-u$ is a unit, and has multiplicative inverse $-v$.
\end{proof}
\begin{lem}
	Let $S$ be a subring of $R$.
	If $S$ has identity, $1_S = 1_R$.
\end{lem}
\begin{proof}
	Choose some $u \in S$.
	$(1_S)(u) = u$.
	Since $1_S, u \in R$, $1_S$ must be the identity in $R$.
\end{proof}
\subsection{3}
\begin{proof}
Let $S$ be a subring of $R$,
and let $u \in S$ be a unit of $S$.
So, $\exists v \in S$ such that $uv = 1_S$.
By the above Lemma, $1_S = 1$
Since $u \in S$, $u \in R$.
Since $v \in S$, $v \in R$.
So $u$ is a unit in $R$.
\end{proof}

\subsection{4}
\begin{proof}
Let $S$ and $T$ be subrings of $R$. Then we know the following:
\begin{align}
	&0_R \in S \land 0_R \in T \label{2s0} \\
	(\forall x_1, x_2 \in S, y_1, y_2 \in T)(&x_1 - x_2 \in S \land y_1 - y_2 \in T) \label{2ss} \\
	(\forall x_1, x_2 \in S, y_1, y_2 \in T)(&x_1 x_2 \in S \land y_1 y_2 \in T) \label{2sm}
\end{align}

Consider $S \cap T$.
Recall that by the definition of intersection:
\begin{equation*}
	z \in S \cap T \iff z \in S \land z \in T
\end{equation*}


By (\ref{2s0}), $0_R \in S \cap T$.

By (\ref{2ss}), $(\forall z_1, z_2 \in S \cap T)(z_1 - z_2 \in S \cap T)$.

By (\ref{2sm}), $(\forall z_1, z_2 \in S \cap T)(z_1 z_2 \in S \cap T)$.

So, $S \cap T$ is a subring of $R$.
\end{proof}

\subsection{5}
\begin{itemize}
	\item[] $A$ and $F$ are subrings of $\mathbb Q$.
	\item[] $B$ is not a subring because $\frac 0 1 \not \in B$.
	\item[] $C$ is not a subring because $(C, +)$ is not a group
		(it is not closed under inverses).
	\item[] $D$ is not a subring because $\frac 1 1 + \frac 1 1 = \frac 2 1 \not \in D$.
	\item[] $E$ is not a subring because $\frac 1 3 + \frac 1 3 = \frac 2 3 \not \in E$.
\end{itemize}
\subsection{7}
\begin{proof}
Let $Z(R)$ denote the center of the ring $R$, and let $a,b \in Z(R)$.  Let $r$ be any element of $R$.

First, since $0_R r = 0_R = r 0_R$, we show $0_R \in Z(R)$.

Since $a$ and $b$ commute (multiplicatively) with $r \in R$:
\begin{equation*}
	(a - b)r = ar - br = ra - rb = r(a - b)
\end{equation*}
This shows that $a - b \in Z(R)$, so $(Z(R), -)$ is closed.

Now let us examine multiplication.
\begin{equation*}
	(ab)r = a(br) = a(rb) = (ar)b = (ra)b = r(ab)
\end{equation*}
This shows that $ab \in Z(R)$, so $(Z(R), *)$ is closed.

Since $Z(R)$ is non-empty and closed under subtraction and multiplication, $Z(R)$ is a ring.

Now $1_R r = r = r 1_R$, so $Z(R)$ has identity.
\end{proof}
\begin{proof}
Let $D$ be a division ring, and $r$ be any element of $D$.

Let $a \in Z(D)$, and $a^{-1}$ be the multiplicative inverse of $a$. Since $a$ commutes with $r$:
\begin{equation*}
	a^{-1}r = a^{-1}raa^{-1} = a^{-1}ara^{-1} = ra^{-1}
\end{equation*}
So $a^{-1} \in Z(D)$, and $Z(D)$ is closed under inverses.
Since all elements in $Z(D)$ commute, $Z(D)$ is commutative.

Thus, $Z(D)$ is a field.
\end{proof}
\subsection{8}
$Z(\mathbb H) = \{a + 0i + 0j + 0k | a \in \mathbb R\}$, or all purely real elements of $\mathbb H$.

\begin{proof}
	Let $S \subseteq \mathbb H $ where $s = \{a + bi | a, b \in \mathbb R\}$. Let $a, b, c, d \in \mathbb R$

	First, $0_\mathbb H = 0 + 0i$, so $0_\mathbb H \in S$.

	Since $(a + bi) - (c + di) = (a - c) + (b - d)i$, $(S, -)$ is closed.

	We can also show the following:
	\begin{align*}
		(a + bi) (c + di)
		&= (ac - bd) + (ad + bc)i \\
		&= (ca - db) + (da - bc)i \\
		&= (c + di)(a + bi)
	\end{align*}
	So $(S, *)$ is both closed and commutative.

	Since ${(a + bi)}^{-1} = (\frac a {a^2 + b^2} - \frac b {a^2 + b^2} i)$, all $x \in S, x \neq 0$ have inverses in $S$.

	Therefore, $S$ is a field.

	However, consider $0 + i \in S$.
	$ij = k$, but $ji = k$. So $i \not \in Z(\mathbb H)$ and $S \not \subseteq Z(\mathbb H)$.
\end{proof}
\subsection{11}
\begin{proof}
Let $R$ be a ring with identity and no zero divisors, and let $x \in R$ such that $x^2 = 1$.
\begin{align*}
	0
	&= 1 + (-1) \\
	&= x^2 + (- 1) \\
	&= x^2 + x + (-x) + (-1) \\
	&= x(x + 1) + (-1)(x + 1) \\
	&= (x - 1)(x + 1)
\end{align*}
Since $R$ has no zero divisors, either $x - 1 = 0$ or $x + 1 = 0$. Thus, $x = \pm 1$.
\end{proof}
\subsection{12}
\begin{proof}
	Let $F$ be a field and $S \subseteq F$ be a subring of $F$ with identity.
	Since $F$ contains no zero divisors, $S$ contains no zero divisors.
	Since $F$ is commutative, $S$ is commutative.
	Since $S$ has identity, no zero divisors, and is commutative, $S$ is an integral domain.
\end{proof}
\subsection{17}
\begin{proof}
	Let $R$ and $S$ be rings. Let us consider $(R \times S)$.

	Since $\exists 0_R \in R$ and $\exists 0_S \in S$, $(0_R, 0_S) \in R \times S$.

	Let $a,c \in R; b,d \in S$. Then $(a, b), (c, d) \in R \times S$

	\begin{equation}\label{dpz}
		(0_R, 0_S) \cdot (a, b) = (0_R a, 0_S b) = (0_R, 0_S)
	\end{equation}
	\begin{equation}\label{dpd}
		(a, b) - (c, d) = (a - c, b - d) \in R \times S
	\end{equation}
	\begin{equation}\label{dpm}
		(a, b) \cdot (c, d) = (ac, bd) \in R \times S
	\end{equation}
	By equations~\ref{dpz},~\ref{dpd}, and~\ref{dpm}, $R \times S$ is a ring
	under componentwise addition and multiplication.

	Let us examine the following logical equivalences:
	\begin{align*}
		(a, b) \cdot (c, d) = (c, d) \cdot (a, b)
		\iff & (ac, bd) = (ca, db) \\
		\iff & ac = ca \land bd = db
	\end{align*}
	This first shows that $(R \times S)$ is commutative if and only if $R$ is commutative and $S$ is commutative.
	\begin{align*}
		& \big(\exists (x, y) \in R \times S\big)\big((a, b) \cdot (x, y) = (x, y) \cdot (a, b) = (a, b)\big) \\
		\iff & (\exists (x, y) \in R \times S)\big((ax, by) = (xa, yb) = (a, b)\big) \\
		\iff & (\exists (x, y) \in R \times S)(ax = xa = a \land by = yb = b) \\
		\iff & (\exists x \in R)(ax = xa = a) \land (\exists y \in S)(by = yb = b)
	\end{align*}
	In the second, we show that $R \times S$ has an identity if and only if both $R$ and $S$ have identity. (We also show that $1_{R \times S} = (1_R, 1_S)$.)
\end{proof}

\subsection{18}
By the previous problem, we know $R \times R$ is a ring, with $0_{R \times R} = (0_R, 0_R)$.

\begin{proof}
Let us consider $S = \{(r, r) : r \in R\}$. First, $(0_R, 0_R) \in S$.

Let $a, b \in R$.
\begin{equation*}
	(a, a) - (b, b) = (a - b, a - b) \in S
\end{equation*}
So $S$ is closed under subtraction.
\begin{equation*}
	(a, a) \cdot (b, b) = (ab, ab) \in S
\end{equation*}
So $S$ is closed under multiplication.
Therefore, $S$ is a subring of $R \times R$.
\end{proof}

\subsection{21}
Let $X$ be any nonempty set and let $\mathcal P (X)$ be the power set of $X$, where
$\forall A, B \in \mathcal P (X)$
\begin{enumerate}[noitemsep]
\item $A + B = (A \setminus B) \cup (B \setminus A)$
\item $A \cdot B = A \cap B$
\end{enumerate}
Let $A, B \in \mathcal P (X)$.
\subsubsection{}
Note that by the definition of a power set, a set $S \in \mathcal P(X) \iff S \subseteq X$.
\begin{lem}
	$( \mathcal P(X), +)$ is an abelian group.
\end{lem}
\begin{proof}
	Consider $A, B \in \mathcal P(X)$.

	\begin{equation}\label{sdz}
		A + \emptyset
		= (A \setminus \emptyset) \cup (\emptyset \setminus A)
		= A \cup \emptyset = A
	\end{equation}
	By equation~\ref{sdz}, we know that $\emptyset$ is the identity.
	Now consider $A + (X \setminus A)$.
	\begin{equation} \label{sdi}
		A + (X \setminus A)
		= A \setminus (X \setminus A) \cup (X \setminus A) \setminus A
		= (A \setminus X) \cup (X \setminus A)
		= \emptyset
	\end{equation}
	By equation~\ref{sdi}, we know that for any element $A \in \mathcal P(X)$,
	$X \setminus A$ is the inverse of $A$.
	\begin{equation*}
		A + B = (A \setminus B) \cup (B \setminus A)
	\end{equation*}
	Since $A \setminus B \subseteq A \subseteq X$
	and $B \setminus A \subseteq B \subseteq X$,
	$A + B \subseteq X$.
	This implies $A + B \subseteq X$.

	Now, for an element to be in the symmetric difference, it is in exactly one of the two sets.
	So, if $x \in (A + B) + C$, then $x$ belongs in exactly one of $A + B$ or $C$.
	If $x$ is not an element of $C$, then $x$ belongs to exactly one of $A$ or $B$.
	If $x$ belongs to $C$, then $x$ is not an element of $A + B$,
	so $x$ is either an element of both $A$ and $B$, or neither $A$ or $B$.

	To write it more simply (where $\veebar$ is the ``exclusive-or'' operator):
	\begin{equation}
	\begin{split}
		\big[ x \in ( A + B ) + C \big]
		\iff & x \in A \setminus B \setminus C \\
		& \veebar x \in B \setminus A \setminus C \\
		& \veebar x \in C \setminus A \setminus B \\
		& \veebar x \in A \cap B \cap C
	\end{split}
	\end{equation}
	This definition is clearly symmetric with regard to $A$, $B$ or $C$,
	so the symmetric difference is commutative and associative.

	Therefore, $(\mathcal P(X), +)$ is an abelian group.
\end{proof}
\begin{proof}
	Let $AB = A \cap B$.
	$(AB)C = A(BC)$ since the intersection is associative,
	and $AB = BA$ since the intersection is commutative.

	Consider $A, B \in \mathcal P(X)$. Then $AB \subseteq A \subseteq X$, so $AB \in \mathcal P(X)$.

	Let $A, B, C \in \mathcal P(X)$.
	\begin{align*}
		(A + B)C = C(A + B)
		= & \big[ (A \setminus B) \cup (B \setminus A) \big] \cap C \\
		= & \big[ (A \setminus B) \cap C \big] \cup \big[ (B \setminus A) \cap C \big] \\
		= & \big[ (A \cap C) \setminus B \big] \cup \big[ (B \cap C) \setminus A \big] \\
		= & AC + BC
	\end{align*}
	Also, $AX = XA = X$, since $A \subseteq X \implies A \cap X = A$,
	and $AA = A$, since $A \cap A = A$.

	So $\mathcal P(X)$ is a boolean ring with identity $X$.
\end{proof}
\subsection{25}
\begin{align*}
	\alpha & = a + bi + cj + dk \\
	\overline \alpha & = a - bi - cj - dk
\end{align*}
Suppose $\alpha$ is a unit in $S$. Then $\alpha^{-1} \in S$, with $\alpha^{-1} = \frac {\overline \alpha} {N(\alpha)}$.
\pagebreak
\section{Chapter 7.3}
\subsection{8}
\begin{enumerate}[label=\textbf{(\alph*)}]
	\item $(a,a) * (r, s) = (ar, as) \not \in I$
	\item $(2a,2b) * (r, s) = (2ar, 2bs) \in I$
	\item $(2a,0) * (r, s) = (2ar, 0) \in I$
	\item $(a,-a) * (b, -b) = (ab, ab) \not \in I$
\end{enumerate}
\subsection{10}
\begin{enumerate}[label=\textbf{(\alph*)}]
\item $x\mathbb Z[x] + 3k$

	$(xp(x) + 3k)(xa(x) + c)$ will have constant term $3ck$.
\item $x^3\mathbb Z[x] + 3c_2x^2 + c_1x + c_0$

	Not an ideal, since $(x^2 )(x) = x^3$.
\item $x^3\mathbb Z[x]$

	$(x^3p(x))q(x) = x^3(p(x)q(x))$
\item $\mathbb Z[x^2]$

	Not an ideal, since $x^2 x = x^3$.
\item Equivalent to $\{f \in \mathbb Z[x] : f(1) = 0\}$.

	$f(1)g(1) = 0g(1) = 0$
\item $\{ p \in \mathbb Z[x] : p'(0) = 0\}$

	Not an ideal, since $\frac d {dx} (1)(x) = 1$.
\end{enumerate}
\subsection{18}
\subsubsection{}
\begin{proof}
	Let $I$ and $J$ be ideals of $R$.
	Let $a,b \in I \cap J$, and $r \in R$.
	\begin{align*}
		a + b \in& I \text{ since $I$ is an ideal} \\
		a + b \in& J \text{ since $J$ is an ideal} \\
		ar \in& I \text{ since $I$ is an ideal} \\
		ar \in& J \text{ since $J$ is an ideal} \\
	\end{align*}
	Therefore $I\cap J$ is an ideal.
\end{proof}
\subsubsection{}
\begin{proof}
	Let $\{I_{i}\}$ be a collection of ideals of $R$.
	Let $a, b \in \bigcap I_i$, and $r \in R$.
	Now, $\forall I \in \{I_i\}$, we have $a,b \in I$. So:
	\begin{align*}
		a + b \in& I \\
		ar \in& I
	\end{align*}
	So $\bigcap I_i$ is an ideal of $R$.
\end{proof}

\subsection{20}
\begin{proof}
Let $I$ be an ideal of $R$ and $S$ be a subring of $R$.
Let $x \in I \cap S$, and $a \in S$.

Since $x \in I$, $ax \in I$.

Since $x, a \in S$, $ax \in S$.
\end{proof}

\subsection{34}
Let $I,J$ be ideals of $R$.
\subsubsection{}
\begin{proof}
	Consider $K$ an ideal of $R$, where $I \subseteq K$ and $J \subseteq K$.
	Consider $a \in I$, and $b \in J$. Since $K$ is closed under addition, $a + b \in K$.
	So $(I + J) \subseteq K$.
\end{proof}
\subsubsection{}
\begin{proof}
	Consider $a \in I$, $b \in J$.
	Since $I$ is an ideal, $ab \in I$.
	Since $J$ is an ideal, $ab \in J$.

	Consider $r \in R$.
	$(ab)r \in I$ and $(ab)r \in J$.
	$(ab) + (cd)$
\end{proof}
\subsubsection{}
$R = \mathbb Z_4$, $I=J=<2>$.
\subsubsection{}
Suppose $R$ is commutative and $I+J=R$.

Let $x \in I \cap J$.
Now for all $r \in R$, $rx \in I \cap J$,
and $\exists a \in I, b \in J: r = a+b$.

$x = 1x = (a+b)x = ax + bx \in IJ$.
\section{Chapter 7.4}
\end{document}
